<?php 

namespace SeedConfirmProEasySlip\Services;

use SeedConfirmProEasySlip\Repositories\EasySlipRepository;
use SeedConfirmProEasySlip\Repositories\MediaRepository;
use SeedConfirmProEasySlip\Repositories\OrderRepository;
use SeedConfirmProEasySlip\Repositories\CarbonRepository;
use SeedConfirmProEasySlip\Repositories\WordPressRepository;

class EasySlipService {
  private $easySlipRepository;
  private $mediaRepository;
  private $wordpressRepository;
  private $orderRepository;
  private $carbonRepository;

  public function __construct() {
    $this->easySlipRepository = new EasySlipRepository(); 
    $this->mediaRepository = new MediaRepository(); 
    $this->orderRepository = new OrderRepository();
    $this->carbonRepository = new CarbonRepository();
    $this->wordpressRepository = new WordPressRepository();
  }

  /**
   * The getSlip function retrieves the slip for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for a specific order. It is used to
   * retrieve the slip for that order.
   */
  public function getSlip($orderId) {
    return $this->carbonRepository->getField($orderId, 'easyslip_image');
  }

  /**
   * The getStatus function retrieves the 'easyslip_status' meta value for a given order ID.
   * 
   * @param orderId The orderId parameter is the unique identifier for the order. It is used to retrieve
   * the status of a specific order.
   * 
   * @return the value of the 'easyslip_status' meta field for the given .
   */
  public function getStatus($orderId) {
    return $this->carbonRepository->getField($orderId, 'easyslip_status');
  }

  /**
   * The getLicense function returns the value of the 'easyslip_option' option from the
   * carbonRepository.
   * 
   * @return the value of the 'easyslip_option' option from the carbon repository.
   */
  public function getLicense() {
    return $this->carbonRepository->getOption('easyslip_license');
  }

  /**
  * The function "verifySlip" verifies a slip for a given order ID and updates the "easyslip_status"
  * field to "verified" using the Carbon repository.
  * 
  * @param orderId The orderId parameter is the unique identifier for the order that needs to be
  * verified.
  */
  public function verifySlip($orderId) {
    $order = wc_get_order($orderId);
    $this->carbonRepository->saveField($orderId, 'easyslip_status', "verified");
  }

  /**
   * The saveSlip function is used to save a slip image for a given order ID.
   * 
   * @param orderId The unique identifier for the order. It could be an integer or a string.
   * @param imageURL The URL of the slip image that needs to be saved.
   */
  public function checkSlip($orderId) {
    $order = wc_get_order($orderId);

    $image = $this->carbonRepository->getField($orderId, 'easyslip_image');
    $bankAccountName = $this->carbonRepository->getField($orderId, 'easyslip_bank_account_name');
    $bankAccountNumber = $this->carbonRepository->getField($orderId, 'easyslip_bank_account_number');

    $existingPayloads = $this->wordpressRepository->getPostMetaValuesFromMetaKeyWithoutPostId("_easyslip_payload", [$orderId]);

    $result = $this->easySlipRepository->verifyPayload($image, $order->total, $bankAccountName, $bankAccountNumber, $existingPayloads);
    $this->carbonRepository->saveField($orderId, 'easyslip_status', $result['status']);
    $this->carbonRepository->saveField($orderId, 'easyslip_payload', $result['payload']);
  }

  /**
   * The saveSlip function is used to save a slip image for a given order ID.
   * 
   * @param orderId The unique identifier for the order. It could be an integer or a string.
   * @param imageURL The URL of the slip image that needs to be saved.
   */
  public function saveSlip($orderId, $file, $bankAccountName, $bankAccountNumber) {
    $order = wc_get_order($orderId);

    $existingPayloads = $this->wordpressRepository->getPostMetaValuesFromMetaKeyWithoutPostId("_easyslip_payload", [$orderId]);

    $image = $this->mediaRepository->upload($file);
    $result = $this->easySlipRepository->verifyPayload($image, $order->total, $bankAccountName, $bankAccountNumber, $existingPayloads);

    $this->carbonRepository->saveField($order->ID, 'easyslip_image', $image);
    $this->carbonRepository->saveField($order->ID, 'easyslip_bank_account_name', $bankAccountName);
    $this->carbonRepository->saveField($order->ID, 'easyslip_bank_account_number', $bankAccountNumber);
    $this->carbonRepository->saveField($order->ID, 'easyslip_status', $result['status']);
    $this->carbonRepository->saveField($order->ID, 'easyslip_payload', $result['payload']);
  }
}