document.addEventListener('submit', function (event) {
  if (event.target.matches('.sc-form')) {
    event.preventDefault();

    var errorLog = [];
    var errorMassage = '';
    var formData = new FormData();
    formData.append('action', 'seed_easyslip');

    var inputData = document.querySelectorAll('.sc-input');
    inputData.forEach(function (el) {
      formData.append(el.name, el.value);
      if (el.dataset.required == 'true' && el.value == '') {
        errorLog.push('true');
        errorMassage += '* ' + el.dataset.massage + '<br>';
      }
    });

    var textareaData = document.querySelectorAll('.sc-textarea');
    textareaData.forEach(function (el) {
      formData.append(el.name, el.value);
      if (el.dataset.required == 'true' && el.value == '') {
        errorLog.push('true');
        errorMassage += '* ' + el.dataset.massage + '<br>';
      }
    });

    var selectData = document.querySelectorAll('.sc-select');
    selectData.forEach(function (el) {
      formData.append(el.name, el.value);
      if (el.dataset.required == 'true' && el.value == '') {
        errorLog.push('true');
        errorMassage += '* ' + el.dataset.massage + '<br>';
      }
    });

    var radioData = document.querySelectorAll('.sc-radio');
    radioData.forEach(function (el) {
      if (el.checked == true) {
        formData.append(el.name, el.value);
        if (el.dataset.required == 'true' && el.value == '') {
          errorLog.push('true');
          errorMassage += '* ' + el.dataset.massage + '<br>';
        }
      }
    });

    var fileData = document.querySelectorAll('.sc-file');
    fileData.forEach(function (el) {
      formData.append(el.name, el.files[0]);
      if (el.dataset.required == 'true' && el.value == '') {
        errorLog.push('true');
        errorMassage += '* ' + el.dataset.massage + '<br>';
      }
    });

    if (errorLog.includes('true')) return;
    
    var ajaxRequest = new XMLHttpRequest();

    ajaxRequest.open('POST', '/wp-admin/admin-ajax.php', true);
    ajaxRequest.send(formData);
  }
});