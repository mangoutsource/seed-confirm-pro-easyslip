<?php
/*
Plugin Name: Seed Confirm Pro with EasySlip 
Description: EasySlip and Seed Confirm Pro seamless integration
Version: 1.0.11
Author: EasySlip
Author URI: https://easyslip.com
Plugin URI: https://easyslip.com
*/

define('SEED_CONFIRM_PRO_EASYSLIP_VERSION', '1.0.11');
define('SEED_CONFIRM_PRO_EASYSLIP_SLUG', 'seed-confirm-pro-easyslip-main');

if (!defined('Carbon_Fields\URL')) {
  define('Carbon_Fields\URL', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'vendor/htmlburger/carbon-fields/' );
}

require 'vendor/autoload.php';

load_plugin_textdomain('seed-confirm-pro-easyslip-main', false, dirname(plugin_basename(__FILE__)) . '/languages/');

use SeedConfirmProEasySlip\Clients\AdminClient;
use SeedConfirmProEasySlip\Clients\HookClient;
use SeedConfirmProEasySlip\Clients\UpdaterClient;

new AdminClient();
new HookClient();
new UpdaterClient();
