<?php 

namespace SeedConfirmProEasySlip\Clients;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Carbon_Fields as CarbonFields;

class AdminClient {
  /**
   * The function registers Carbon Fields in WordPress.
   */
  public function __construct() {
    add_action('carbon_fields_register_fields', array($this, 'register_carbon_fields'));
    add_action('carbon_fields_register_fields', array($this, 'register_order_customer_fields'));
    add_action('plugins_loaded', [$this, 'init']);
  }

  /**
   * The init function initializes the CarbonFields library in PHP.
   */
  public function init() {
    CarbonFields::boot();
  }

  /**
   * The function "register_carbon_fields" registers a text field for the "License" option under the
   * "License: EasySlip" theme options container, with the parent page set to "options-general.php".
   */
  public function register_carbon_fields() {
    $fields = [
      Field::make('text', 'easyslip_license', __('License', 'seed-confirm-pro-easyslip-main')),
    ];

    Container::make('theme_options', __('EasySlip: Seed Confirm Pro', 'seed-confirm-pro-easyslip-main'))->add_fields($fields)->set_page_parent('options-general.php');
  }

  /**
   * The function "register_order_customer_fields" creates custom fields for the "order" post type,
   * specifically for EasySlip related information.
   */
  public function register_order_customer_fields() {
    $fields = [
      Field::make('text', 'easyslip_image', __('EasySlip Image', 'seed-confirm-pro-easyslip-main')),
      Field::make('text', 'easyslip_status', __('EasySlip Status', 'seed-confirm-pro-easyslip-main')),
      Field::make('text', 'easyslip_payload', __('EasySlip Payload', 'seed-confirm-pro-easyslip-main')),
      Field::make('text', 'easyslip_bank_account_name', __('EasySlip Bank Account Name', 'seed-confirm-pro-easyslip-main')),
      Field::make('text', 'easyslip_bank_account_number', __('EasySlip Bank Account Number', 'seed-confirm-pro-easyslip-main')),
    ];

    Container::make('post_meta', __('Seed Confirm Pro EasySlip', 'seed-confirm-pro-easyslip-main'))->where('post_type', '=', 'order')->add_fields($fields);
  }
}