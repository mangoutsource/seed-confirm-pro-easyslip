<?php 

namespace Tests;

use PHPUnit\Framework\TestCase;
use Repositories\EasySlipRepository;

class EasySlipHelperTest extends TestCase
{
  public function testCheckMatchShouldBeMatched(): void
  {
    $targetNameTh = "นางสุชาดา ความสุข";
    $targetNumber = "1234532736";
    $targetAmount = "409";
    $existingPayloads = [];

    $data = '{
      "status": 200,
      "data": {
        "payload": "0041000600000101030040220013320142247CTF040155102TH910419B8",
        "transRef": "013320142247CTF04015",
        "date": "2023-11-16T14:22:47+07:00",
        "countryCode": "TH",
        "amount": {
          "amount": 409,
          "local": {
            "amount": 409,
            "currency": "764"
          }
        },
        "fee": 0,
        "ref1": "",
        "ref2": "",
        "ref3": "",
        "sender": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาย ภิรัช จ",
              "en": "MR. Bhiraj J"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x5020-x"
            }
          }
        },
        "receiver": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาง สุชาดา ค",
              "en": "MRS. SUCHADA K"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x3273-x"
            }
          }
        }
      }
    }';

    $data = json_decode($data);

    $repo = new EasySlipRepository();
    $result = $repo->checkMatch($data, $targetAmount, $targetNameTh, $targetNumber, $existingPayloads);
    $this->assertSame("verified", $result);
  }

  public function testCheckMatchShouldBeMismatched(): void
  {
    $targetNameTh = "นางสุชาดา ความสุข";
    $targetNameEn = "MRS. SUCHADA K";
    $targetNumber = "1234532236";
    $targetAmount = "409";
    $existingPayloads = [];

    $data = '{
      "status": 200,
      "data": {
        "payload": "0041000600000101030040220013320142247CTF040155102TH910419B8",
        "transRef": "013320142247CTF04015",
        "date": "2023-11-16T14:22:47+07:00",
        "countryCode": "TH",
        "amount": {
          "amount": 409,
          "local": {
            "amount": 409,
            "currency": "764"
          }
        },
        "fee": 0,
        "ref1": "",
        "ref2": "",
        "ref3": "",
        "sender": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาย ภิรัช จ",
              "en": "MR. Bhiraj J"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x5020-x"
            }
          }
        },
        "receiver": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาง สุชาดา ค",
              "en": "MRS. SUCHADA K"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x3273-x"
            }
          }
        }
      }
    }';

    $data = json_decode($data);

    $repo = new EasySlipRepository();
    $result = $repo->checkMatch($data, $targetAmount, $targetNameTh, $targetNumber, $existingPayloads);
    $this->assertSame('invalid_bank', $result);
  }

  public function testWhenSenderIsKrungsriShouldBeMatched(): void
  {
    $targetNameTh = "นาย ภูมิชนะ อุดแก้ว";
    $targetNumber = "123-824-0472";
    $targetAmount = "190";
    $existingPayloads = [];
    
    $data = '{
      "status": 200,
      "data": {
        "payload": "0046000600000101030250225KMA231124123723UWMWL7YLfv5102TH9104F095",
        "transRef": "KMA231124123723UWMWL7YLfv",
        "date": "2023-11-24T12:37:17+07:00",
        "countryCode": "TH",
        "amount": {
          "amount": 190,
          "local": {
            "amount": 0,
            "currency": "764"
          }
        },
        "fee": 0,
        "ref1": "",
        "ref2": "",
        "ref3": "นาย ภูมิชนะ อุดแก้ว;",
        "sender": {
          "bank": {
            "id": "025",
            "name": "ธนาคารกรุงศรีอยุธยา",
            "short": "BAY"
          },
          "account": {
            "name": {
              "th": "PHUMCHANA U",
              "en": "PHUMCHANA U"
            },
            "bank": {
              "type": "BANKAC",
              "account": "XXX-1-16615-X"
            }
          }
        },
        "receiver": {
          "bank": {
            "id": "014",
            "name": "ธนาคารไทยพาณิชย์",
            "short": "SCB"
          },
          "account": {
            "name": {
              "th": "PHUMCHANA U",
              "en": "PHUMCHANA U"
            },
            "bank": {
              "type": "BANKAC",
              "account": "XXXXX4047X"
            }
          }
        }
      }
    }';

    $data = json_decode($data);

    $repo = new EasySlipRepository();
    $result = $repo->checkMatch($data, $targetAmount, $targetNameTh, $targetNumber, $existingPayloads);
    $this->assertSame('verified', $result);
  }

  public function testWhenSlipDuplicatedShouldBeMismatched(): void
  {
    $targetNameTh = "นางสุชาดา ความสุข";
    $targetNumber = "1234532736";
    $targetAmount = "409";
    $existingPayloads = ['0041000600000101030040220013320142247CTF040155102TH910419B8'];

    $data = '{
      "status": 200,
      "data": {
        "payload": "0041000600000101030040220013320142247CTF040155102TH910419B8",
        "transRef": "013320142247CTF04015",
        "date": "2023-11-16T14:22:47+07:00",
        "countryCode": "TH",
        "amount": {
          "amount": 409,
          "local": {
            "amount": 409,
            "currency": "764"
          }
        },
        "fee": 0,
        "ref1": "",
        "ref2": "",
        "ref3": "",
        "sender": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาย ภิรัช จ",
              "en": "MR. Bhiraj J"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x5020-x"
            }
          }
        },
        "receiver": {
          "bank": {
            "id": "004",
            "name": "ธนาคารกสิกรไทย",
            "short": "KBANK"
          },
          "account": {
            "name": {
              "th": "นาง สุชาดา ค",
              "en": "MRS. SUCHADA K"
            },
            "bank": {
              "type": "BANKAC",
              "account": "xxx-x-x3273-x"
            }
          }
        }
      }
    }';

    $data = json_decode($data);

    $repo = new EasySlipRepository();
    $result = $repo->checkMatch($data, $targetAmount, $targetNameTh, $targetNumber, $existingPayloads);
    $this->assertSame("duplicated_slip", $result);
  }
}