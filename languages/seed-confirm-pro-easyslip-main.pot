#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Seed Confirm Pro with EasySlip\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-02 11:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.6.6; wp-6.4.1\n"
"X-Domain: seed-confirm-pro-easyslip-main"

#. Author of the plugin
#: clients/hook.client.php:78
msgid "EasySlip"
msgstr ""

#. Description of the plugin
msgid "EasySlip and Seed Confirm Pro seamless integration"
msgstr ""

#: clients/admin.client.php:47
msgid "EasySlip Bank Account Name"
msgstr ""

#: clients/admin.client.php:48
msgid "EasySlip Bank Account Number"
msgstr ""

#: clients/admin.client.php:44
msgid "EasySlip Image"
msgstr ""

#: clients/admin.client.php:46
msgid "EasySlip Payload"
msgstr ""

#: clients/admin.client.php:45
msgid "EasySlip Status"
msgstr ""

#: clients/admin.client.php:35
msgid "EasySlip: Seed Confirm Pro"
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "https://easyslip.com"
msgstr ""

#: clients/hook.client.php:152
msgid "Invalid"
msgstr ""

#: clients/admin.client.php:32
msgid "License"
msgstr ""

#: clients/hook.client.php:170
msgid "Mark as verified"
msgstr ""

#: clients/hook.client.php:173
msgid "Recheck"
msgstr ""

#: clients/admin.client.php:51
msgid "Seed Confirm Pro EasySlip"
msgstr ""

#. Name of the plugin
msgid "Seed Confirm Pro with EasySlip"
msgstr ""

#: clients/hook.client.php:166
msgid "status:"
msgstr ""

#: clients/hook.client.php:147
msgid "Valid"
msgstr ""
